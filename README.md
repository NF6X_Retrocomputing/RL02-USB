# RL02-USB: Convert a DEC RL02 into a Ginormous USB Hard Drive

## Introduction

The goal of this project is to modify a Digital Equipment Corporation
(DEC) RL02 hard disk drive into a comically oversized USB hard drive.
It is not the first such project in the world; prior art includes
[this 2015 project by Christopher Parish](https://github.com/ChrisPVille/RL02).

The RL02 was used with many DEC PDP-11 and VAX computer systems. It is
a rack-mountable hard disk drive with a single, removable, 14"
platter, and a capacity of 10 megabytes. The platter is formatted with
512 cylinders, and 40 sectors per track. Each sector holds 128 16-bit
words of data, and the media is hard-sectored via notches machined
into the disk pack's hub.

Up to four RL02 drives could be connected in daisy-chain fashion to a
single controller card, using long cables and a resistive terminator
at the end of the chain. Each individual drive contains servo logic
and related low-level functions, while the common controller card
contains the logic necessary to read and write data sectors.

This project is a new controller card which mounts on the rear panel
of a single RL02 drive in place of its original drive interface bus
connectors, and provides a USB mass storage interface to the drive.

Now, why would anybody do something so silly? Well, my goal is to use
this modified drive for two purposes:

1. Image existing RL02 packs in my collection for archival and backup
   purposes.

2. Write new images onto RL02 packs in order to install new operating
   systems and software on my vintage computers.

After contemplating a project like this on and off over a number of
years, I finally decided to take on the project for
[Retrochallenge 2016/10](http://retrochallenge.org).

## Project Status

It was my stated goal
to work on this project during October, 2016 for
[Retrochallenge 2016/10](http://retrochallenge.org), but I did not
realistically expect to complete this project within a month.

Latest status update:

* I have completed the design of the main controller PCB.

* I have also completed the design of a small bus terminator PCB.

* I have created a design for a 3D printable case.

* FPGA design not yet started.

* MCU firmware development not yet started.

## Planned Features

* Single main controller board mounts on rear panel of RL02 drive in place
  of original drive bus interface connectors.

* Requires 6-20 VDC power, tapped from RL02 power supply's +V UN REG
  output, which is nominally around 16 VDC.

* USB 2.0 HS interface.

* Presents RL02 media as a USB mass storage device. Block size is 256
  bytes.

* LCD character display inconveniently mounted on rear of drive
  provides status information.

* Disk controller functions to be implemented in Xilinx Spartan-6 FPGA.

* USB interface functions to be implemented in STM32F4 series
  microcontroller.

* 14 pin, 2mm pitch FPGA JTAG header. Use Xilinx Platform Cable,
  Digilent JTAG-HS3, etc.

* 20 pin, 0.1" pitch microcontroller debug interface header. Use
  ST-Link/v2 or equivalent.

* Main controller board connects to original RL02 logic card in place
  of original drive bus, using 40 pin ribbon cable.

* Two headers provide 8 debugging outputs each from the FPGA and MCU,
  for connection to a logic analyzer.

## References

Reference documents found at the following URLs are archived in the
*references* directory for convenience.

* [RL01/RL02 Disk Drive Technical Manual](http://bitsavers.trailing-edge.com/pdf/dec/disc/rl01_rl02/EK-RL012-TM-PRE_RL01_RL02_Disk_Drive_Technical_Manual_Preliminary_Dec79.pdf)

* [RL02 Field Maintenance Print Set](http://bitsavers.trailing-edge.com/pdf/dec/disc/rl01_rl02/MP00553_RL02_RevB_Engineering_Drawings_Nov79.pdf)

* [RL11 Field Maintenance Print Set](http://bitsavers.trailing-edge.com/pdf/dec/unibus/MP00153_RL11EngrDrws.pdf)

* [FPGA based DEC RL01/RL01 Disk-Drive Simulator V.3.03](http://fafner.dyndns.org/~heuberger/DE0RL/README33.pdf)

## Component Datasheets

Component datasheets relevant to this project are archived in the
*datasheets* directory for convenience.

## Copyright and Licensing

RL02-USB Copyright (C) 2016 Mark J. Blair <nf6x@nf6x.net>

Released under GPLv3

Files in the *datasheets* and *references* directories are included
for convenience. They are copyrighted by their respective creators,
and are not covered by the GPLv3 license under which I am releasing my
original creation.
