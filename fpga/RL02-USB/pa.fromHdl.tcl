
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name RL02-USB -dir "/home/mblair/RL02-USB/fpga/RL02-USB/planAhead_run_3" -part xc6slx9tqg144-2
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "RL02_USB.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {RL02_USB.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top RL02_USB $srcset
add_files [list {RL02_USB.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx9tqg144-2
