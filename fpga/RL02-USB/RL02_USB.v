`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////
// This file is part of RL02-USB.
//
// RL02-USB is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// RL02-USB is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with RL02-USB.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////


module RL02_USB(
    // Clock and reset inputs
    input REFCLK,                               // Reference clock, 65.6 MHz
    input FRESETn,                              // Master FPGA reset

    // MCU interface bus
    input [15:0] A,                             // Address bus
    inout [7:0] D,                              // Data bus
    input CS1n,                                 // Chip select, registers
    input CS2n,                                 // Chip select, RAM buffers
    input OEn,                                  // Output enable
    input WEn,                                  // Write enable
    output FINT,                                // Interrupt output


    // RL02 drive interface
    output ACLO,                                // AC voltage low
    output [1:0] DS,                            // Drive select
    output WG,                                  // Write gate
    output SYSCLK,                              // System clock, 4.1 MHz
    output WDATA,                               // Write data
    output DRVCMD,                              // Drive command
    input RDATA,                                // Read data
    input STATIN,                               // Status input
    input STATCLK,                              // Status clock
    input SECPLS,                               // Sector pulse
    input DRVRDY,                               // Drive ready
    input DRVERR,                               // Drive error

    // Debug header
    output [7:0] FDEBUG                         // Debug outputs
    );


    reg [3:0] sysclk_div;                       // SYSCLK divider counter
    

    // Tie off unimplemented outputs
    assign FINT   = 1'b0;
    assign ACLO   = 1'b0;
    assign DS     = 2'b00;
    assign WG     = 1'b0;
    assign WDATA  = 1'b0;
    assign DRVCMD = 1'b0;
    assign FDEBUG = 8'h00;


    // Generate 4.1 MHz RL02 system clock by dividing 65.6 MHz REFCLK
    initial
        sysclk_div = 4'h0;

    always @(posedge REFCLK)
        sysclk_div <= sysclk_div + 1;

    assign SYSCLK = sysclk_div[3];

endmodule
